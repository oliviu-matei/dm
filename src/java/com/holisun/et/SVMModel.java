/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.holisun.et;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ApplicationScoped;

import com.rapidminer.Process;
import com.holisun.cs.DMRun;
import com.rapidminer.operator.Operator;
import java.io.Serializable;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "svmModelBean")
@ApplicationScoped
public class SVMModel implements Serializable {

    private String name;
    private long id;

    private String templateName;
    private String pes;

    private List<String> kernelType;
    private List<String> startPopulationType;
    private List<String> mutationType;
    private List<String> selectionType;

    private String selectedStartPopulationType;
    private String selectedKernelType;
    private String selectedMutationType;
    private String selectedSelectionType;

    private double kernelSigma1;
    private double kernelShift;
    private double c;
    private double epsilon;
    private double holdOutSetRatio;
    private double crossoverProb;

    private int maxGenerations;
    private int generationsWithoutImproval;
    private int populationSize;
    private int tournamentFraction;

    private boolean keepBbest;
    private boolean useLlocalRandomSeed;
    private boolean showConvergencePlot;
    private boolean showPopulationPlot;
    private boolean returnOptimizationPerformance;
    
    private boolean showParam1;
    private boolean showParam2;

    //<editor-fold desc="LABELS" defaultstate="collapsed">
    private String TXT_kernelType = "kernel_type";
    private String TXT_kernelSigma1 = "kernel_sigma1";
    private String TXT_kernelShift = "kernel_shift";
    private String TXT_epsilon = "epsilon";
    private String TXT_c = "C";
    private String TXT_holdOutSetRation = "hold_out_set_ration";
    private String TXT_crossoverProb = "crossover_prob";
    private String TXT_maxGenerations = "max_generations";
    private String TXT_generationsWithoutImproval = "generations_without_improval";
    private String TXT_populationSize = "population_size";
    private String TXT_tournamentFraction = "tournament_fraction";
    private String TXT_keepBest = "keep_best";
    private String TXT_useLocalRandomSeed = "use_local_random_seed";
    private String TXT_showConvergenvePlot = "show_convergence_plot";
    private String TXT_returnOptimizationPerformance = "return_optimization_performance";
    private String TXT_startPopulationType = "start_population_type";
    private String TXT_mutationType = "mutation_type";
    private String TXT_selectionType = "selection_type";
//</editor-fold>

    @PostConstruct
    public void init() {
        name = "svm";
        kernelType = new ArrayList<String>();
        kernelType.add("dot");
        kernelType.add("radial");
        kernelType.add("polinomial");
        kernelType.add("sigmoid");
        kernelType.add("anova");
        kernelType.add("epanechnikov");
        kernelType.add("multiquadric");

        startPopulationType = new ArrayList<String>();
        startPopulationType.add("random");
        startPopulationType.add("min");
        startPopulationType.add("max");

        mutationType = new ArrayList<String>();
        mutationType.add("none");
        mutationType.add("gaussian_mutation");
        mutationType.add("switching_mutation");
        mutationType.add("sparsity_mutation");

        selectionType = new ArrayList<String>();
        selectionType.add("uniform");
        selectionType.add("cut");
        selectionType.add("roulette wheel");
        selectionType.add("stochastic universal sampling");
        selectionType.add("Boltzmann");
        selectionType.add("rank");
        selectionType.add("tournament");
        selectionType.add("non dominated sorting");

    }

    public String verify() {

        return "";
    }

    public boolean isShowParam1() {
        return ! getSelectedKernelType().equals("dot");
    }

    public void setShowParam1(boolean showParam1) {
        this.showParam1 = showParam1;
    }

    public boolean isShowParam2() {
        return getSelectedKernelType().equals("polynomial") || getSelectedKernelType().equals("sigmoid") ||
                getSelectedKernelType().equals("anova") || getSelectedKernelType().equals("epanechnikov") ||
                getSelectedKernelType().equals("multiquadric");
    }

    public void setShowParam2(boolean showParam2) {
        this.showParam2 = showParam2;
    }
    
    

    public String loadFile() {
//        setKernelShift(3.2);
//        try {
//            Process process = DMRun.open(getTestingFile());
//            Operator op = process.getOperator("SWM");
//
//            setKernelSigma1(op.getParameterAsDouble("kernel_sigma1"));
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            setKernelSigma1(5.5);
//        } finally {
//            setKernelSigma1(1.5);
//        }
//        
        return "";
    }

// <editor-fold desc="GET - SET" defaultstate="collapsed">
    public List<String> getStartPopulationType() {
        return startPopulationType;
    }

    public long getId() {
        return id;
    }

    public void setPes(String pes) {
        this.pes = pes;
    }

    public String getPes() {
        return pes;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTXT_epsilon() {
        return TXT_epsilon;
    }

    public String getTXT_mutationType() {
        return TXT_mutationType;
    }

    public String getTXT_selectionType() {
        return TXT_selectionType;
    }

    public String getTXT_c() {
        return TXT_c;
    }

    public String getTXT_kernelType() {
        return TXT_kernelType;
    }

    public List<String> getKernelType() {
        return kernelType;
    }

    public String getTXT_startPopulationType() {
        return TXT_startPopulationType;
    }

    public List<String> getMutationType() {
        return mutationType;
    }

    public String getSelectedStartPopulationType() {
        return selectedStartPopulationType;
    }

    public String getSelectedKernelType() {
        return selectedKernelType;
    }

    public String getSelectedMutationType() {
        return selectedMutationType;
    }

    public String getSelectedSelectionType() {
        return selectedSelectionType;
    }

    public double getC() {
        return c;
    }

    public double getEpsilon() {
        return epsilon;
    }

    public double getCrossoverProb() {
        return crossoverProb;
    }

    public void setKernelType(List<String> kernelType) {
        this.kernelType = kernelType;
    }

    public void setStartPopulationType(List<String> startPopulationType) {
        this.startPopulationType = startPopulationType;
    }

    public void setMutationType(List<String> mutationType) {
        this.mutationType = mutationType;
    }

    public void setSelectionType(List<String> selectionType) {
        this.selectionType = selectionType;
    }

    public void setSelectedStartPopulationType(String selectedStartPopulationType) {
        this.selectedStartPopulationType = selectedStartPopulationType;
    }

    public void setSelectedKernelType(String selectedKernelType) {
        this.selectedKernelType = selectedKernelType;
    }

    public void setSelectedMutationType(String selectedMutationType) {
        this.selectedMutationType = selectedMutationType;
    }

    public void setSelectedSelectionType(String selectedSelectionType) {
        this.selectedSelectionType = selectedSelectionType;
    }

    public void setC(double c) {
        this.c = c;
    }

    public void setEpsilon(double epsilon) {
        this.epsilon = epsilon;
    }

    public void setCrossoverProb(double crossoverProb) {
        this.crossoverProb = crossoverProb;
    }

    public List<String> getSelectionType() {
        return selectionType;
    }

    public double getKernelSigma1() {
        return kernelSigma1;
    }

    public double getKernelShift() {
        return kernelShift;
    }

    public double getHoldOutSetRatio() {
        return holdOutSetRatio;
    }

    public int getGenerationsWithoutImproval() {
        return generationsWithoutImproval;
    }

    public int getPopulationSize() {
        return populationSize;
    }

    public int getTournamentFraction() {
        return tournamentFraction;
    }

    public boolean isKeepBbest() {
        return keepBbest;
    }

    public boolean isUseLlocalRandomSeed() {
        return useLlocalRandomSeed;
    }

    public boolean isShowConvergencePlot() {
        return showConvergencePlot;
    }

    public boolean isShowPopulationPlot() {
        return showPopulationPlot;
    }

    public boolean isReturnOptimizationPerformance() {
        return returnOptimizationPerformance;
    }

    public String getTXT_kernelSigma1() {
        if (getSelectedKernelType().equals("polynomial")) {
            return "kernel_degree";
        } else if (getSelectedKernelType().equals("sigmoid")) {
            return "kernel_a";
        } else if (getSelectedKernelType().equals("anova")) {
            return "kernel_gamma";
        } else if (getSelectedKernelType().equals("epanechnikov")) {
            return "kernel_sigma1";
        } else if (getSelectedKernelType().equals("multiquadric")) {
            return "kernel_sigma1";
        }
        
        return TXT_kernelSigma1;
    }

    public String getTXT_kernelShift() {
        if (getSelectedKernelType().equals("polynomial")) {
            return "kernel_shift";
        } else if (getSelectedKernelType().equals("sigmoid")) {
            return "kernel_b";
        } else if (getSelectedKernelType().equals("anova")) {
            return "kernel_degree";
        } else if (getSelectedKernelType().equals("epanechnikov")) {
            return "kernel_degree";
        } else if (getSelectedKernelType().equals("multiquadric")) {
            return "kernel_shift";
        }
        
        return TXT_kernelShift;
    }

    public String getTXT_holdOutSetRation() {
        return TXT_holdOutSetRation;
    }

    public String getTXT_crossoverProb() {
        return TXT_crossoverProb;
    }

    public String getTXT_maxGenerations() {
        return TXT_maxGenerations;
    }

    public void setMaxGenerations(int maxGenerations) {
        this.maxGenerations = maxGenerations;
    }

    public int getMaxGenerations() {
        return maxGenerations;
    }

    public String getTXT_generationsWithoutImproval() {
        return TXT_generationsWithoutImproval;
    }

    public String getTXT_populationSize() {
        return TXT_populationSize;
    }

    public String getTXT_tournamentFraction() {
        return TXT_tournamentFraction;
    }

    public String getTXT_keepBest() {
        return TXT_keepBest;
    }

    public String getTXT_useLocalRandomSeed() {
        return TXT_useLocalRandomSeed;
    }

    public String getTXT_showConvergenvePlot() {
        return TXT_showConvergenvePlot;
    }

    public String getTXT_returnOptimizationPerformance() {
        return TXT_returnOptimizationPerformance;
    }

    public void setKernelSigma1(double kernelSigma1) {
        this.kernelSigma1 = kernelSigma1;
    }

    public void setKernelShift(double kernelShift) {
        this.kernelShift = kernelShift;
    }

    public void setHoldOutSetRatio(double holdOutSetRatio) {
        this.holdOutSetRatio = holdOutSetRatio;
    }

    public void setGenerationsWithoutImproval(int generationsWithoutImproval) {
        this.generationsWithoutImproval = generationsWithoutImproval;
    }

    public void setPopulationSize(int populationSize) {
        this.populationSize = populationSize;
    }

    public void setTournamentFraction(int tournamentFraction) {
        this.tournamentFraction = tournamentFraction;
    }

    public void setKeepBbest(boolean keepBbest) {
        this.keepBbest = keepBbest;
    }

    public void setUseLlocalRandomSeed(boolean useLlocalRandomSeed) {
        this.useLlocalRandomSeed = useLlocalRandomSeed;
    }

    public void setShowConvergencePlot(boolean showConvergencePlot) {
        this.showConvergencePlot = showConvergencePlot;
    }

    public void setShowPopulationPlot(boolean showPopulationPlot) {
        this.showPopulationPlot = showPopulationPlot;
    }

    public void setReturnOptimizationPerformance(boolean returnOptimizationPerformance) {
        this.returnOptimizationPerformance = returnOptimizationPerformance;
    }

//</editor-fold>
}
