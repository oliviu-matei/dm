/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.holisun.et;

import com.holisun.cs.DMiner;
import com.rapidminer.RapidMiner;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.operator.IOContainer;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.nio.CSVExampleSource;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.jws.WebParam;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "storeModelBean")
@ViewScoped
public class StoreModel implements Serializable {
    
    private long id;

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }
    private String algorithm;
    private String xml;
    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
    

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public void init() {
    }
    

    public String storeProcessTemplate() {
        try {
            File newFile = new File(algorithm + ".rmp");
            FileWriter fout = new FileWriter(newFile);
            fout.write(xml);
            fout.close();
            setResult(xml);

        } catch (Exception ex) {
            ex.printStackTrace();
            return ex.getMessage();
        }
        return "";
    }

    public String readProcessTemplate() {
        DMiner miner = new DMiner();
        setResult(miner.readProcessTemplate(algorithm));
        return "";
    }
}
