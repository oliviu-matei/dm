/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.holisun.et;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "knnModelBean")
@ApplicationScoped
public class KNNModel implements Serializable {

    private long id = 100;
    private String name;

    private String pes;
    private int k;
    private boolean weightedVote;

    private String TXT_k = "k";
    private String MT_MixedMeasures = "mixed_measure";
    private String MT_NominalMeasures = "nominal_measure";
    private String MT_NumericalMeasures = "numerical_measure";
    private String MT_BregmanDivergences = "divergences";
    private String TXT_measureTypes = "measure_types";
    private String TXT_weightedVote = "weighted_vote";

    public String getTXT_weightedVote() {
        return TXT_weightedVote;
    }

    public void setTXT_weightedVote(String TXT_weightedVote) {
        this.TXT_weightedVote = TXT_weightedVote;
    }
        
    String selectedMeasureType = MT_NominalMeasures;

    public String getSelectedMeasureTypeType() {
        return selectedMeasureTypeType;
    }

    public void setSelectedMeasureTypeType(String selectedMeasureTypeType) {
        this.selectedMeasureTypeType = selectedMeasureTypeType;
    }

    public List<String> getMeasureTypesTypes() {
        measureTypesTypes.clear();
        if (selectedMeasureType.equals(MT_BregmanDivergences)) {
            measureTypesTypes.add("GenerelizedDivergence");
            measureTypesTypes.add("ItakuraSaitoDivergence");
            measureTypesTypes.add("KLDivergence");
            measureTypesTypes.add("LogisticLoss");
            measureTypesTypes.add("MahalanobisDistance");
        } else if (selectedMeasureType.equals(MT_MixedMeasures)) {
            measureTypesTypes.add("MixedEuclideanDistance");
        } else if (selectedMeasureType.equals(MT_NominalMeasures)) {
            measureTypesTypes.add("NominalDistance");
            measureTypesTypes.add("DiceSimilarity");
            measureTypesTypes.add("JaccardSimilarity");
            measureTypesTypes.add("SimpleMatchingSimilarity");
        } else {
            measureTypesTypes.add("EuclideanDistance");
            measureTypesTypes.add("CamberraDistance");
            measureTypesTypes.add("ChebychevDistance");
            measureTypesTypes.add("CosineSimilarity");
            measureTypesTypes.add("NominalDistance");
            measureTypesTypes.add("DiceSimilarity");
            measureTypesTypes.add("JaccardSimilarity");
            measureTypesTypes.add("ManhattanDistance");
        } 

        return measureTypesTypes;
    }

    public void setMeasureTypesTypes(List<String> measureTypesTypes) {
        this.measureTypesTypes = measureTypesTypes;
    }
    List<String> measureTypes;

    String selectedMeasureTypeType;
    List<String> measureTypesTypes;

    @PostConstruct
    public void init() {
        name = "knn";
        measureTypes = new ArrayList<String>();
        measureTypes.add(MT_MixedMeasures);
        measureTypes.add(MT_NominalMeasures);
        measureTypes.add(MT_NumericalMeasures);
        measureTypes.add(MT_BregmanDivergences);
        
        measureTypesTypes = new ArrayList<String>();

    }
// <editor-fold desc="GET - SET" defaultstate="collapsed">
    public String getTXT_measureTypes() {
        return TXT_measureTypes;
    }

    public boolean isWeightedVote() {
        return weightedVote;
    }

    public void setWeightedVote(boolean weightedVote) {
        this.weightedVote = weightedVote;
    }

    
    public void setSelectedMeasureType(String selectedMeasureType) {
        this.selectedMeasureType = selectedMeasureType;
    }

    public long getId() {
        return id;
    }

    public String getSelectedMeasureType() {
        return selectedMeasureType;
    }

    public String getPes() {
        return pes;
    }

    public void setPes(String pes) {
        this.pes = pes;
    }

    public int getK() {
        return k;
    }

    public void setK(int k) {
        this.k = k;
    }

    public List<String> getMeasureTypes() {
        return measureTypes;
    }

    public void setMeasureTypes(List<String> measureTypes) {
        this.measureTypes = measureTypes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTXT_k() {
        return TXT_k;
    }
    
    //</editor-fold>
}
