/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.holisun.et;

import com.holisun.cs.DMiner;
import com.rapidminer.RapidMiner;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.operator.IOContainer;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.nio.CSVExampleSource;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ApplicationScoped;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "indexModelBean")
@ApplicationScoped
public class IndexModel implements Serializable {

    @ManagedProperty(value="#{knnModel}")
    private KNNModel knnModel;

    @ManagedProperty(value="#{lprModel}")
    private LPRModel lprModel;

    @ManagedProperty(value="#{svmModel}")
    private SVMModel svmModel;


    private String TXT_separator = "column_separator";
    private String TXT_dateFormat = "date_format";
    private String TXT_windowSize = "window_size";
    private String TXT_stepSize = "step_size";
    private String TXT_horizon = "horizon";

    private long id = 15;
    private String name;
    private String pes;
    private String selected;
    private int windowSize;
    private int stepSize;
    private int horizon;
    private String separator = ";";
    private String result;
    private List<String> dataTypes;
    private List<String> columnTypes;
    private String dateFormat = "MM.dd.yy";
    private List<String> columns;
    private List<TblRow> csvRows;

    private String learningFile = "http://devel-teglas.holisun.com/dminer/Cojocna_procesat_training.csv";
    private String testingFile = "http://devel-teglas.holisun.com/dminer/Cojocna_procesat_testing.csv";
    private String outputFile = "";
    private String templateName;

    private List<String> templates;

    private String rmp;

    String ALG_KNN = "k-Nearest Neighbour (k-NN)";
    String ALG_LPR = "Local Polynomial Regression";
    String ALG_SVM = "Support Vector Machine (SVM)";

    
    public KNNModel getKnnModel() {
        return knnModel;
    }

    public void setKnnModel(KNNModel knnModel) {
        this.knnModel = knnModel;
    }

    public SVMModel getSvmModel() {
        return svmModel;
    }

    public void setSvmModel(SVMModel svmModel) {
        this.svmModel = svmModel;
    }

    public LPRModel getLprModel() {
        return lprModel;
    }

    public void setLprModel(LPRModel lprModel) {
        this.lprModel = lprModel;
    }

    
    public String getTXT_separator() {
        return TXT_separator;
    }

    public void setTXT_separator(String TXT_separator) {
        this.TXT_separator = TXT_separator;
    }

    public String getTXT_windowSize() {
        return TXT_windowSize;
    }

    public void setTXT_windowSize(String TXT_windowSize) {
        this.TXT_windowSize = TXT_windowSize;
    }

    public String getTXT_stepSize() {
        return TXT_stepSize;
    }

    public void setTXT_stepSize(String TXT_stepSize) {
        this.TXT_stepSize = TXT_stepSize;
    }

    public String getTXT_horizon() {
        return TXT_horizon;
    }

    public void setTXT_horizon(String TXT_horizon) {
        this.TXT_horizon = TXT_horizon;
    }

    public List<String> getDataTypes() {
        return dataTypes;
    }

    public void setDataTypes(List<String> dataTypes) {
        this.dataTypes = dataTypes;
    }

    public List<String> getColumnTypes() {
        return columnTypes;
    }

    public void setColumnTypes(List<String> columnTypes) {
        this.columnTypes = columnTypes;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public List<String> getColumns() {
        return columns;
    }

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }

    public List<TblRow> getCsvRows() {
        return csvRows;
    }

    public void setCsvRows(List<TblRow> csvRows) {
        this.csvRows = csvRows;
    }

    public String getTXT_dateFormat() {
        return TXT_dateFormat;
    }

    public void setTXT_dateFormat(String TXT_dateFormat) {
        this.TXT_dateFormat = TXT_dateFormat;
    }


    public int getWindowSize() {
        return windowSize;
    }

    public void setWindowSize(int windowSize) {
        this.windowSize = windowSize;
    }

    public int getStepSize() {
        return stepSize;
    }

    public void setStepSize(int stepSize) {
        this.stepSize = stepSize;
    }

    public int getHorizon() {
        return horizon;
    }

    public void setHorizon(int horizon) {
        this.horizon = horizon;
    }

    public String getSeparator() {
        return separator;
    }

    public void setSeparator(String separator) {
        this.separator = separator;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getOutputFile() {
        return outputFile;
    }

    public void setOutputFile(String outputFile) {
        this.outputFile = outputFile;
    }

    public String getRmp() {
        return rmp;
    }

    public void setRmp(String rmp) {
        this.rmp = rmp;
    }
    
    public void init() {
        templates = new ArrayList<String>();
        templates.add(ALG_KNN);
        templates.add(ALG_LPR);
        templates.add(ALG_SVM);
        dataTypes = new ArrayList<String>();
        dataTypes.add("date");
        dataTypes.add("time");
        dataTypes.add("date_time");
        dataTypes.add("numeric");
        dataTypes.add("integer");
        dataTypes.add("real");
        dataTypes.add("text");
        columnTypes = new ArrayList<String>();
        columnTypes.add("attribute");
        columnTypes.add("label");
        columnTypes.add("id");
        //csvRows = new ArrayList<TblRow>();
        
        /*@@@
        knnModel.init();
        lprModel.init();
        svmModel.init();
        */
    }

    public String getPes() {
        return pes;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public void setPes(String pes) {
        this.pes = pes;
    }

    public String getLearningFile() {
        return learningFile;
    }

    public void setLearningFile(String learningFile) {
        this.learningFile = learningFile;
    }

    public String getTestingFile() {
        return testingFile;
    }

    public void setTestingFile(String testingFile) {
        this.testingFile = testingFile;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
        if (selected.equals(ALG_KNN)) {
            this.setTemplateName("knn.xhtml");
        } else if (selected.equals(ALG_LPR)) {
            this.setTemplateName("lpr.xhtml");
        } else {
            this.setTemplateName("svm.xhtml");
        }
    }

    public List<String> getTemplates() {
        return templates;
    }

    public void setTemplates(List<String> templates) {
        this.templates = templates;
    }

    public String trimite() {
        return "";
    }
    
    public void setWindowingParams(Operator op) {
        op.setParameter(TXT_windowSize, getWindowSize() + "");
        op.setParameter(TXT_stepSize, getStepSize() + "");
        op.setParameter(TXT_horizon, getHorizon() + "");
    }
    
    public String loadFile() { 
        try {
            URL learningUrl = new URL(getLearningFile());
            BufferedReader br = new BufferedReader(new InputStreamReader(learningUrl.openStream()));
            String line = br.readLine();
            String[] c = line.split(getSeparator());
            this.columns = new ArrayList<String>(Arrays.asList(c));
            URL testingUrl = new URL(getTestingFile());
            BufferedReader bt = new BufferedReader(new InputStreamReader(testingUrl.openStream()));
            String tLine = bt.readLine();
            String[] tc = tLine.split(getSeparator());
            List ltc = new ArrayList<String>(Arrays.asList(tc));
            if (!columns.equals(ltc)) {
                setResult("The two files do not have the same column structure");
            } else {
                csvRows = new ArrayList<>();
                for (int i = 0; i < columns.size(); i++) {
                csvRows.add(new TblRow(columns.get(i) + ""));
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
            setResult(ex.getMessage());
        }
        return "";
    }

    private void setReadCSVParameters(com.rapidminer.Process process, String operatorName) {
        Operator op = process.getOperator("Read CSV");
        op.setParameter(CSVExampleSource.PARAMETER_CSV_FILE, getLearningFile());
        op.setParameter(TXT_separator, getSeparator());
        op.setParameter(TXT_dateFormat, getDateFormat());
        List<String[]> list = new ArrayList<String[]>();
        for (int i = 0; i < getCsvRows().size(); i++) {
            TblRow tr = (TblRow)getCsvRows().get(i);
            String s[] = {i + "", tr.getColumn() + ".true."+ tr.getSelectedDataType() + "." + tr.getSelectedColumnType()};
            list.add(s);
        }
        op.setListParameter("data_set_meta_data_information", list);
    } 
    
    private com.rapidminer.Process createProcess() {
        com.rapidminer.Process process = null;
        DMiner miner = new DMiner();
        
        String text = miner.readProcessTemplate(name);
        setResult(text);
        try {
            RapidMiner.setExecutionMode(RapidMiner.ExecutionMode.COMMAND_LINE);
            RapidMiner.init();
            process = new com.rapidminer.Process(text);
            
            setReadCSVParameters(process, "Read CSV");
            setReadCSVParameters(process, "Read CSV (2)");
                        
            Operator op3;

            if (selected.equals(ALG_LPR)) {
                op3 = process.getOperator("Local Polynomial Regression");
                op3.setParameter(lprModel.getTXT_degree(), lprModel.getDegree() + "");
                op3.setParameter(lprModel.getTXT_distance(), lprModel.getDistance() + "");
                op3.setParameter(lprModel.getTXT_atLeast(), lprModel.getAtLeast() + "");
                op3.setParameter(lprModel.getTXT_fixedDistance(), lprModel.getFixedDistance() + "");
                op3.setParameter(lprModel.getTXT_k(), lprModel.getK() + "");
                op3.setParameter(lprModel.getTXT_neighbourhoodType(), lprModel.getSelectedNeighbourhoodType() + "");
                op3.setParameter(lprModel.getTXT_numericalMeasure(), lprModel.getSelectedNumericalMeasure() + "");
                op3.setParameter(lprModel.getTXT_relativeSize(), lprModel.getRelativeSize() + "");
                op3.setParameter(lprModel.getTXT_ridgeFactor(), lprModel.getRidgeFactor() + "");
                op3.setParameter(lprModel.getTXT_smoothingKernel(), lprModel.getSelectedSmoothingKernel() + "");
            } else if (selected.equals(ALG_KNN)) {
                op3 = process.getOperator("k-NN");
                op3.setParameter(knnModel.getTXT_k(), knnModel.getK() + "");
                op3.setParameter(knnModel.getTXT_weightedVote(), knnModel.isWeightedVote()+ "");
                op3.setParameter(knnModel.getTXT_measureTypes(), knnModel.getSelectedMeasureType());
                op3.setParameter(knnModel.getSelectedMeasureType(), knnModel.getSelectedMeasureTypeType());
            } else if (selected.equals(ALG_SVM)) {
                op3 = process.getOperator("SVM");
                //op3.setParameter(knnModel.getTXT_k(), knnModel.getK() + "");
            } 
                
            setWindowingParams(process.getOperator("Windowing"));
            setWindowingParams(process.getOperator("Windowing (2)"));            
            
            IOContainer ioResult = process.run();

            //setResult(process.toString());

        } catch (Exception ex) {
            setResult(ex.getMessage());
        }
        return process;
    }

    public String storePES() {
        try {
            com.rapidminer.Process process = createProcess();
            if (process != null) {
                File newFile = new File(getPes() + ".rmp");
                FileWriter fout = new FileWriter(newFile);
                fout.write(process.toString());
                fout.close();
                //setResult(process.toString());
                // setResult("PES " + indexModel.getPes() + " stored successfully");
            } else {
                //setResult("Oops! PES " + getPes() + " could not be created");
            }
        } catch (Exception ex) {
            setResult(ex.getMessage());
        }
        return "";
    }

    public String testPES() {
        try {
            com.rapidminer.Process process = createProcess();
            IOContainer ioResult = process.run();
            setResult(ioResult.getElementAt(2) + "");
        } catch (Exception ex) {
            setResult(ex.getMessage());
        }
        return "";
    }

    public String runPES() {
        try {
            com.rapidminer.Process process = createProcess();
            IOContainer ioResult = process.run();
            ExampleSet resultSet = (ExampleSet)ioResult.getElementAt(1);
            setResult(ioResult.getElementAt(1) + "");
        } catch (Exception ex) {
            setResult(ex.getMessage());
        }
        return "";
    }


}
