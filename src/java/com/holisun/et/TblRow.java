/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.holisun.et;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class TblRow implements Serializable {
    
    public TblRow(String column) {
        this.column = column;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getSelectedDataType() {
        return selectedDataType;
    }

    public void setSelectedDataType(String selectedDataType) {
        this.selectedDataType = selectedDataType;
    }

    public String getSelectedColumnType() {
        return selectedColumnType;
    }

    public void setSelectedColumnType(String selectedColumnType) {
        this.selectedColumnType = selectedColumnType;
    }
    
    
    private String column;
    private String selectedDataType;
    private String selectedColumnType;
    
    
}
