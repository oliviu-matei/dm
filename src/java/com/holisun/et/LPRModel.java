/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.holisun.et;

import com.holisun.cs.DMiner;
import com.rapidminer.RapidMiner;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.operator.IOContainer;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.nio.CSVExampleSource;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ApplicationScoped;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "lprModelBean")
@ApplicationScoped
public class LPRModel implements Serializable {

    private long id;
    private String name;

    private String selected;
    
    private String templateName;
    private List<String> templates;

    private int degree;
    private double ridgeFactor;
    private List<String> numericalMeasure;
    private List<String> smoothingKernel;
    private List<String> neighbourhoodType;

    private double relativeSize;
    private String selectedSmoothingKernel;
    private String selectedNumericalMeasure;
    private String selectedNeighbourhoodType;
    private String result;

    private int k;
    private double fixedDistance;
    private double distance;
    private int atLeast;



    //<editor-fold desc="LABELS" defaultstate="collapsed">
    private String TXT_degree = "degree";
    private String TXT_ridgeFactor = "ridge_factor";
    private String TXT_numericalMeasure = "numerical_measure";
    private String TXT_neighbourhoodType = "neighboorhood_type";
    private String TXT_k = "k";
    private String TXT_fixedDistance = "fixed_distance";
    private String TXT_distance = "distance";
    private String TXT_atLeast = "at_least";
    private String TXT_relativeSize = "relative_size";
    private String TXT_smoothingKernel = "smoothing_kernel";


    

    //</editor-fold>
    // <editor-fold desc="CONSTANTS" defaultstate="collapsed">
    private String NM_DiceSimilarity = "DiceSimilarity";
    private String NM_DynamicTimeWarpingDistance = "DynamicTimeWarpingDistance";
    private String NM_InnerProductSimilarity = "InnerProductSimilarity";
    private String NM_JaccardSimilarity = "JaccardSimilarity";
    private String NM_KernelEuclideanDistance = "KernelEuclideanDistance";
    private String NM_ManhattanDistance = "ManhattanDistance";
    private String NM_OverlapSimilarity = "OverlapSimilarity";
    private String NM_MaxProductSimilarity = "MaxProductSimilarity";

    private String NT_Fixed_Number = "Fixed Number";
    private String NT_Fixed_Distance = "Fixed Distance";
    private String NT_Relative_Number = "Relative Number";
    private String NT_Distance_but_at_least = "Distance but at least";

    private String SK_Rectangular = "Rectangular";
    private String SK_Triangular = "Triangular";
    private String SK_Epanechnikov = "Epanechnikov";
    private String SK_Bisquare = "Bisquare";
    private String SK_Tricube = "Tricube";
    private String SK_Gaussian = "Gaussian";
    private String SK_Exponential = "Exponential";
    private String SK_McLain = "McLain";

    String ALG_KNN = "k-Nearest Neighbour (k-NN)";
    String ALG_LPR = "Local Polynomial Regression";
    String ALG_SVM = "Support Vector Machine (SVM)";

    //</editor-fold>
    @PostConstruct
    public void init() {
        name = "lpr";
        numericalMeasure = new ArrayList<String>();
        numericalMeasure.add(NM_DiceSimilarity);
        numericalMeasure.add(NM_DynamicTimeWarpingDistance);
        numericalMeasure.add(NM_InnerProductSimilarity);
        numericalMeasure.add(NM_JaccardSimilarity);
        numericalMeasure.add(NM_KernelEuclideanDistance);
        numericalMeasure.add(NM_ManhattanDistance);
        numericalMeasure.add(NM_OverlapSimilarity);
        numericalMeasure.add(NM_MaxProductSimilarity);

        neighbourhoodType = new ArrayList<String>();
        neighbourhoodType.add(NT_Fixed_Number);
        neighbourhoodType.add(NT_Fixed_Distance);
        neighbourhoodType.add(NT_Relative_Number);
        neighbourhoodType.add(NT_Distance_but_at_least);

        smoothingKernel = new ArrayList<>();
        smoothingKernel.add(SK_Rectangular);
        smoothingKernel.add(SK_Triangular);
        smoothingKernel.add(SK_Epanechnikov);
        smoothingKernel.add(SK_Bisquare);
        smoothingKernel.add(SK_Tricube);
        smoothingKernel.add(SK_Gaussian);
        smoothingKernel.add(SK_Exponential);
        smoothingKernel.add(SK_McLain);

        templates = new ArrayList<String>();
        templates.add(ALG_KNN);
        templates.add(ALG_LPR);
        templates.add(ALG_SVM);

    }

    
//<editor-fold desc="GET - SET" defaultstate="collapsed">
    public void setSelected(String selected) {
        this.selected = selected;
        if (selected.equals(ALG_KNN)) {
            this.setTemplateName("knn.xhtml");
        } else if (selected.equals(ALG_LPR)) {
            this.setTemplateName("lpr.xhtml");
        } else {
            this.setTemplateName("svm.xhtml");
        }
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public String getSelected() {
        return selected;
    }

    public String getTemplateName() {
        return templateName;
    }

    public List<String> getTemplates() {
        return templates;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public void setTemplates(List<String> templates) {
        this.templates = templates;
    }

    /*
    public void setPes(String pes) {
        this.pes = pes;
    }
*/
    public double getDistance() {
        return distance;
    }

    public long getId() {
        return id;
    }

    public int getDegree() {
        return degree;
    }

    public void setDegree(int degree) {
        this.degree = degree;
    }

    public double getRidgeFactor() {
        return ridgeFactor;
    }

    public List<String> getSmoothingKernel() {
        return smoothingKernel;
    }

    public void setSmoothingKernel(List<String> smoothingKernel) {
        this.smoothingKernel = smoothingKernel;
    }

    public String getSelectedSmoothingKernel() {
        return selectedSmoothingKernel;
    }

    public String getTXT_smoothingKernel() {
        return TXT_smoothingKernel;
    }

    public void setSelectedSmoothingKernel(String selectedSmoothingKernel) {
        this.selectedSmoothingKernel = selectedSmoothingKernel;
    }

    public void setRidgeFactor(double ridgeFactor) {
        this.ridgeFactor = ridgeFactor;
    }

    public String getSelectedNumericalMeasure() {
        return selectedNumericalMeasure;
    }

    public void setSelectedNumericalMeasure(String selectedNumericalMeasure) {
        this.selectedNumericalMeasure = selectedNumericalMeasure;
    }

    public List<String> getNumericalMeasure() {
        return numericalMeasure;
    }

    public void setNumericalMeasure(List<String> numericalMeasure) {
        this.numericalMeasure = numericalMeasure;
    }

    public String getSelectedNeighbourhoodType() {
        return selectedNeighbourhoodType;
    }

    public void setSelectedNeighbourhoodType(String selectedNeighbourhoodType) {
        this.selectedNeighbourhoodType = selectedNeighbourhoodType;
    }

    public List<String> getNeighbourhoodType() {
        return neighbourhoodType;
    }

    public void setNeighbourhoodType(List<String> neighbourhoodType) {
        this.neighbourhoodType = neighbourhoodType;
    }

    public double getRelativeSize() {
        return relativeSize;
    }

    public void setRelativeSize(double relativeSize) {
        this.relativeSize = relativeSize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getK() {
        return k;
    }

    public double getFixedDistance() {
        return fixedDistance;
    }

    public int getAtLeast() {
        return atLeast;
    }

    public String getTXT_degree() {
        return TXT_degree;
    }

    public String getTXT_ridgeFactor() {
        return TXT_ridgeFactor;
    }

    public String getTXT_numericalMeasure() {
        return TXT_numericalMeasure;
    }

    public String getTXT_neighbourhoodType() {
        return TXT_neighbourhoodType;
    }

    public String getTXT_k() {
        return TXT_k;
    }

    public String getTXT_fixedDistance() {
        return TXT_fixedDistance;
    }

    public String getTXT_distance() {
        return TXT_distance;
    }

    public String getTXT_atLeast() {
        return TXT_atLeast;
    }

    public String getTXT_relativeSize() {
        return TXT_relativeSize;
    }

    public void setK(int k) {
        this.k = k;
    }

    public void setFixedDistance(double fixedDistance) {
        this.fixedDistance = fixedDistance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public void setAtLeast(int atLeast) {
        this.atLeast = atLeast;
    }

    //</editor-fold>
}
