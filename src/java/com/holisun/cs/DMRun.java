/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.holisun.cs;

import java.io.File;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

import com.rapidminer.RapidMiner;
import com.rapidminer.Process;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.operator.IOContainer;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.nio.CSVExampleSource;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.Writer;

/**
 *
 * @author Admin
 */
@WebService(serviceName = "DMRun")
public class DMRun {

    @WebMethod(operationName = "run")
    public String run(@WebParam(name = "pes") String pes) {
        try {
            RapidMiner.setExecutionMode(RapidMiner.ExecutionMode.COMMAND_LINE);
            RapidMiner.init();
            Process process = new Process(new File("pes"));
            Operator op = process.getOperator("Read CSV");
            op.setParameter(CSVExampleSource.PARAMETER_CSV_FILE, "fileD:\\cojocna_procesat_training.csv");
            Operator op2 = process.getOperator("Read CSV (2)");
            op2.setParameter("csv_file", "D:\\cojocna_procesat_testing.csv");
            Operator op3 = process.getOperator("Local Polynomial Regression");
            op3.setParameter("degree", "2");

            IOContainer ioResult = process.run();
            ExampleSet resultSet1 = (ExampleSet) ioResult.getElementAt(0);
            System.out.println(ioResult);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    @WebMethod(operationName = "store")
    public String store(@WebParam(name = "pes") String pes, @WebParam(name = "rmp") String rmp) {
        try {
            File newFile = new File(pes);
            FileWriter fout = new FileWriter(newFile);
            fout.write(rmp);
            fout.close();

        } catch (Exception ex) {
            ex.printStackTrace();
            return ex.getMessage();
        }
        return "";
    }

    @WebMethod(operationName = "storeProcessTemplate")
    public String storeProcessTemplate(@WebParam(name = "algorithm") String algorithm, @WebParam(name = "rmp") String rmp) {
        try {
            File newFile = new File(algorithm + ".rmp");
            FileWriter fout = new FileWriter(newFile);
            fout.write(rmp);
            fout.close();

        } catch (Exception ex) {
            ex.printStackTrace();
            return ex.getMessage();
        }
        return "";
    }

    @WebMethod(operationName = "readProcessTemplate")
    public String readProcessTemplate(@WebParam(name = "algorithm") String algorithm) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(algorithm + ".rmp")); 
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            String everything = sb.toString();
            return everything;
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    @WebMethod(operationName = "operation")
    public String operation(@WebParam(name = "file") String file) {
        try {
            RapidMiner.setExecutionMode(RapidMiner.ExecutionMode.COMMAND_LINE);
            RapidMiner.init();
            Process process = new Process(new File("d:\\lpr.rmp"));
            Operator op = process.getOperator("Read CSV");
            op.setParameter(CSVExampleSource.PARAMETER_CSV_FILE, "fileD:\\cojocna_procesat_training.csv");
            Operator op2 = process.getOperator("Read CSV (2)");
            op2.setParameter("csv_file", "D:\\cojocna_procesat_testing.csv");
            Operator op3 = process.getOperator("Local Polynomial Regression");
            op3.setParameter("degree", "2");

            IOContainer ioResult = process.run();
            ExampleSet resultSet1 = (ExampleSet) ioResult.getElementAt(0);
            System.out.println(ioResult);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

}
