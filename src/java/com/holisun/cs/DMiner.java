/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.holisun.cs;

import java.io.BufferedReader;
import java.io.FileReader;

import com.rapidminer.RapidMiner;
import com.rapidminer.Process;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.operator.IOContainer;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.nio.CSVExampleSource;
import java.io.File;
import java.io.FileWriter;

/**
 *
 * @author Admin
 */
public class DMiner {

    public DMiner() {
        
    }

    public String readProcessTemplate(String algorithm) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(algorithm + ".rmp"));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            String everything = sb.toString();
            return everything;
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }
    

}
